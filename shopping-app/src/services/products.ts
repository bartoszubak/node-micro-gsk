import { Product } from "../db/models/product"



export const getProducts = async (filter) => {
  return Product.findAll({})
}

export const getProductById = async (product_id) => {
  return Product.findByPk(product_id)
}


export interface CreateProductPayload {
  name: string;
  description: string;
  price: number;
}

export const createProduct = async (productPaylod: CreateProductPayload) => {
  const { description, name, price } = productPaylod
  return Product.create({
    description, name, price,
  })
}
