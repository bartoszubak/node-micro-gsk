'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {

    return queryInterface.bulkInsert('Products', [

      {
        "id": "b54a57c8-f993-4b84-890e-8209a37893a7",
        "name": "Testproduct",
        "description": "opis",
        "price": 10.1,
        "createdAt": "2020-10-22T10:40:18.279Z",
        "updatedAt": "2020-10-22T10:40:18.279Z"
      },
      {
        "id": "ca7b1155-9a29-43ee-bfd8-11a32e5c1d14",
        "name": "Test product 2",
        "description": "opis",
        "price": 10.1,
        "createdAt": "2020-10-22T10:40:59.890Z",
        "updatedAt": "2020-10-22T10:40:59.890Z"
      }
    ])
  },

  down: async (queryInterface, Sequelize) => {
    await queryInterface.bulkDelete('Products', null, {});
  }
};
