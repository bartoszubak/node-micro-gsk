'use strict';
import Sequelize, { DataTypes, Model, Optional, UUIDV4 } from 'sequelize';
import { sequelize } from '..';

// These are all the attributes in the User model
interface ProductAttributes {
  id: string;
  name: string;
  description: string;
  price: number;
  createdAt?: string
  updatedAt?: string
}

// Some attributes are optional in `Product.build` and `Product.create` calls
interface ProductCreationAttributes extends Optional<ProductAttributes, "id"> { }


export class Product extends Model<ProductAttributes, ProductCreationAttributes> implements ProductAttributes {
  id: string;
  name: string;
  description: string;
  price: number;
  createdAt!: string;
  updatedAt!: string;
  /**
   * Helper method for defining associations.
   * This method is not a part of Sequelize lifecycle.
   * The `models/index` file will call this method automatically.
   */
  static associate(models) {
    // define association here
  }
};

Product.init({
  id: {
    allowNull: false,
    primaryKey: true,
    type: Sequelize.UUID,
    defaultValue: UUIDV4
  },
  name: DataTypes.STRING,
  description: DataTypes.TEXT,
  price: DataTypes.FLOAT
}, {
  sequelize,
  modelName: 'Product',
});
