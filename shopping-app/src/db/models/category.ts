import { Model, DataTypes, Optional, Sequelize, UUIDV4 } from "sequelize";
import { sequelize } from "..";

interface CategoryAttributes {
  id: string;
  name: string;
  desc: string;
  parentId?: string;
  createdAt?: string
  updatedAt?: string
}

interface CategoryCreationAttributes extends Optional<CategoryAttributes, "id"> { }

export class Category extends Model<CategoryAttributes,CategoryCreationAttributes> implements CategoryAttributes {
  id!: string;
  name: string;
  desc: string;
  parentId?: string;
}

Category.init({
  id: {
    primaryKey: true,
    type: DataTypes.UUID,
    defaultValue: UUIDV4 // Or Sequelize.UUIDV1
  },
  name: DataTypes.STRING,
  desc: DataTypes.STRING,
  parentId: {
    type: DataTypes.UUID,
    allowNull: true
  },
}, {
  sequelize,
  modelName: 'Category',
  timestamps: true,
  tableName: 'Categories'
});
