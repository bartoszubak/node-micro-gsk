const  {connect} = require('node-nats-streaming')

const stan = connect('test-cluser', 'pub1', {
  url: 'http://localhost:4222/'
})

stan.on('connect', () => {
  stan.publish('posts:created', JSON.stringify({ test: 123 }), () => {
    console.log('Event sent!')
  })
})


process.on('SIGHUP', () => {
  stan.close()
  process.exit(0)
})