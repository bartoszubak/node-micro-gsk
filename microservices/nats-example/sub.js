const { connect } = require('node-nats-streaming')

const stan = connect('test-cluser', 'sub1', {
  url: 'http://localhost:4222/'
})

const options = stan.subscriptionOptions()
  .setManualAckMode(true)
  .setDeliverAllAvailable()
  .setDurableName('my-service-queue')

stan.on('connect', () => {

  const subscription = stan.subscribe('posts:^', /* 'service-group', */ options)

  subscription.on('message', (msg) => {
    console.log('Event #' + msg.getSequence())
    console.log(msg.getData())
  })
})



process.on('SIGHUP', () => {
  stan.close()
  process.exit(0)
})